package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.R.integer;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.model.EmployBean;
import com.UHF.model.MyAdapter;
import com.UHF.turntable.CircleActivity;
import com.UHF.turntable.ScanActivity;
import com.UHF.util.CheckDataFromServer;
import org.apache.commons.lang.StringUtils;


public class MaterialOutActivity extends ScanActivity implements OnClickListener{
	//标题
	private TextView mo_title;
	/**
	 * 0:入托，1托盘出库2物料出库
	 */
	private String type="0";
	/**
	 * 按钮类型//1物料2托盘3库位
	 */
	private String lastBt="-1";
	
	private String operateType="0";
	
	private ListView listView;
	private AcceptanceAdapter myAdapter;
	private CheckBoxAdapter checkAdapter;
	private Map<String,Integer> data=new HashMap<String, Integer>();
	private Map<String,Integer> oldData=new HashMap<String, Integer>();
	//个数
	private TextView allCounts;
	//个数
	private Integer counts= 0;
	/**
	 * 新扫描确认的数据
	 */
	private List<String> materialAddData=new ArrayList<String>();
	/**
	 * 删除的数据
	 */
	private  List<String> materialDelData=new ArrayList<String>();
	/**
	 * 原始数据
	 */
	private LinkedHashMap<Object,Object> materialOriginData=new LinkedHashMap<Object,Object>();
	/**
	 * epcid/物料流水号
	 */
	private LinkedHashMap<Object,Object> materialListData=new LinkedHashMap<Object,Object>();
	
	/**
	 * 物料流水号.状态
	 */
	private LinkedHashMap<Object,Object> materiaCheckData=new LinkedHashMap<Object,Object>();
	private Handler mHandler;
//	private String documentNo;
//	private Intent intent;
//	private String goodsName;
//	private String goodsCode;
//	private String demandCount;
//	private String assignCount;
	private String outboundDemandId;
	private Button refresh;
	private Button mo_scanMaterial_bt;//扫物料
	private Button mo_checkIt_bt;//确定
	private Button deleteAssignPackage;//删除
	private String extendOrderCode;
	private String issueCode;
	private String orderState;
	private String extendingMaterialId;


	@Override
	protected void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.material_out_package_list);
//		goodsName=getIntent().getStringExtra("goodsName");
//		goodsCode=getIntent().getStringExtra("goodsCode");
//		demandCount=getIntent().getStringExtra("demandCount");
//		assignCount=getIntent().getStringExtra("assignCount");
		outboundDemandId=getIntent().getStringExtra("outboundDemandId");
		extendOrderCode=getIntent().getStringExtra("extendOrderCode");
		issueCode=getIntent().getStringExtra("issueCode");
		orderState=getIntent().getStringExtra("orderState");
		extendingMaterialId=getIntent().getStringExtra("extendingMaterialId");
		
		
		mo_scanMaterial_bt = (Button)findViewById(R.id.mo_scanMaterial_bt);
		mo_scanMaterial_bt.setOnClickListener(this);
		
		mo_checkIt_bt = (Button)findViewById(R.id.mo_checkIt_bt);
		mo_checkIt_bt.setOnClickListener(this);
		
		deleteAssignPackage = (Button)findViewById(R.id.deleteMoPackage);
		deleteAssignPackage.setOnClickListener(this);
		
		refresh = (Button)findViewById(R.id.mo_refresh);
		refresh.setOnClickListener(this);

		mo_title = (TextView)findViewById(R.id.mo_title);//
		mo_title.setText("物料出库--包装列表:"+extendOrderCode);
		listView = (ListView)findViewById(R.id.materialPackageList);//
		allCounts = (TextView)findViewById(R.id.allCounts1);//
			mHandler = new Handler(){
			@Override
			public void handleMessage(Message msg) {
				// TODO Auto-generated method stub
	    		super.handleMessage(msg);
				switch (msg.what) {
				case 0:
					if(isCanceled) return;
					break;
				case 1:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					if(!checkMaterial(data)){
						List checkList=checkMapData(oldData,data);
						if(checkList.size()>0){
							CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
							String s=StringUtils.join(checkList.toArray(), ",");
							Map m=new HashMap();
							m.put("scanCode", s);
							m.put("extendingMaterialId", extendingMaterialId);
							m.put("user_id", UfhData.getEmployBean().getId());
							checkDataFromServer.setOperType("15");
							checkDataFromServer.setData(m);
							checkDataFromServer.setIp(UfhData.getIP());
							checkDataFromServer.setmHandler(handler);
							checkDataFromServer.setWhat(1);
							oldData.putAll(data);
							if(!checkDataFromServer.checkData()){//此处有问题
								oldData.remove(data);
							}
						}
					}else{
						if(checkAdapter == null){
							checkAdapter = new CheckBoxAdapter(MaterialOutActivity.this, materiaCheckData);
							listView.setAdapter(checkAdapter);
						}else{
							checkAdapter.setData(materiaCheckData) ;
						}
						allCounts.setText(counts+"");
						checkAdapter.notifyDataSetChanged();
					}
					break;
				case 2:
					/*if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					if("0".equals(type)){
						List checkList=checkMapData(oldData,data);
						if(checkList.size()>0){
							CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
							String s=StringUtils.join(checkList.toArray(), ",");
							Map m=new HashMap();
							m.put("scanCode", s);
							m.put("user_id", UfhData.getEmployBean().getId());
							checkDataFromServer.setOperType("2");
							checkDataFromServer.setData(m);
							checkDataFromServer.setIp(UfhData.getIP());
							checkDataFromServer.setmHandler(handler);
							checkDataFromServer.setWhat(2);
							if(checkDataFromServer.checkData()){
								oldData.putAll(data);
							}
						}
					}else if("1".equals(type)){
						checkTray(data);
						if(myAdapter == null){
							myAdapter = new AcceptanceAdapter(ProductOutActivity.this, trayCheckData);
							listView.setAdapter(myAdapter);
						}else{
							myAdapter.setData(trayCheckData) ;
						}
						myAdapter.notifyDataSetChanged();
					}*/
					break;
				case 3:
					if(isCanceled) return;
					data = UfhData.scanResult6c;
					if(data==null||data.isEmpty()){
						break;
					}
					List checkList=checkMapData(oldData,data);
					if(checkList.size()>0){
						CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
						String s=StringUtils.join(checkList.toArray(), ",");
						Map m=new HashMap();
						m.put("scanCode", s);
						m.put("user_id", UfhData.getEmployBean().getId());
//						m.put("type", "0");//解绑扫物料
						checkDataFromServer.setOperType("3");
						checkDataFromServer.setData(m);
						checkDataFromServer.setIp(UfhData.getIP());
						checkDataFromServer.setmHandler(handler);
						checkDataFromServer.setWhat(3);
						oldData.putAll(data);
						if(!checkDataFromServer.checkData()){//此处有问题
							oldData.remove(data);
						}
					}
					break;
				case 4:
					try {
						lastBt="-1";
						String s=(String) msg.obj;
						if(s==null||"1".equals(s)){
							Toast.makeText(MaterialOutActivity.this,"物料出库失败!",Toast.LENGTH_SHORT).show();
						}else{
							try {
								String re="";
								JSONObject ss= new JSONObject(s);
								String operType=ss.getString("operType");
								String result=ss.getString("result");
								if("3".equals(result)){
									Toast.makeText(MaterialOutActivity.this,"保存成功",Toast.LENGTH_SHORT).show();
									onBackPressed();
								}else if("1".equals(result)){
									Toast.makeText(MaterialOutActivity.this,"保存失败",Toast.LENGTH_SHORT).show();
								}else if("XX".equals(result)){
									Toast.makeText(MaterialOutActivity.this,"违反不同生产商禁混的混批原则,请确认",Toast.LENGTH_SHORT).show();
								}else if("No Authority".equals(result)){
									Toast.makeText(MaterialOutActivity.this,"当前用户无该包装的操作权限",Toast.LENGTH_SHORT).show();
								}else {
									Toast.makeText(MaterialOutActivity.this,ss.getString("warningMessage"),Toast.LENGTH_SHORT).show();
								}
							} catch (Exception e) {
								// TODO: handle exception
								Toast.makeText(MaterialOutActivity.this,"保存失败",Toast.LENGTH_SHORT).show();
							}
						}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(MaterialOutActivity.this,"物料出库失败!",Toast.LENGTH_SHORT).show();
					}
					break;
				default:
					break;
				}
				
			}
			
		};
		refreshList();
	}
	
	public boolean refreshList(){
		materialListData.clear();
		materiaCheckData.clear();
		materialOriginData.clear();
		materialListData.clear();
		materiaCheckData.clear();
		materialAddData.clear();
		materialDelData.clear();
		data.clear();
		oldData.clear();
		checkAdapter= null;
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
//		m.put("goodsName", goodsName);//货物名称
//		m.put("goodsCode", goodsCode);//货物编号
//		m.put("demandCount", demandCount);//需求量
//		m.put("assignCount", assignCount);//实发量
		m.put("extendingMaterialId", extendingMaterialId);//物料单据id
//		m.put("extendingMaterialId", extendingMaterialId);//物料单据id
		m.put("user_id", UfhData.getEmployBean().getId());
		checkDataFromServer.setOperType("5");//物料出库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}

	public boolean deletePackage(){
		List<String> packageInventoryNums=checkAdapter.getChecked();
		if(packageInventoryNums.size()>0){
			for(String p:packageInventoryNums){
				Object m=materialOriginData.get(p);
				if(m!=null){
					String scanCode=(String) ((Map) m).get("scanCode");
					if(materialAddData.contains(scanCode)){
						materialAddData.remove(scanCode);
					}else{
						materialDelData.add(scanCode);
					}
                	materiaCheckData.remove(materialListData.get(scanCode));
                	materialListData.remove(scanCode);
				}
			}
			checkAdapter=null;
			if(checkAdapter == null){
				checkAdapter = new CheckBoxAdapter(MaterialOutActivity.this, materiaCheckData);
				listView.setAdapter(checkAdapter);
			}else{
				checkAdapter.setData(materiaCheckData) ;
			}
			allCounts.setText(counts+"");
			checkAdapter.setChecked(new ArrayList());
			checkAdapter.notifyDataSetChanged();
			return true;
		}else{
			Toast.makeText(MaterialOutActivity.this,"请选中要删除的包装!",Toast.LENGTH_SHORT).show();
		}
		return false;
		
	}
	
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		switch (v.getId()) {
		case R.id.mo_scanMaterial_bt://扫物料
			if("1".equals(lastBt)){
				lastBt="-1";
				mo_scanMaterial_bt.setText(R.string.scan);
				closeRFIDScan();
				data.clear();
				oldData.clear();
			}else if("-1".equals(lastBt)){
				lastBt="1";//查物料
				mo_scanMaterial_bt.setText(R.string.stop);
				openRFIDScan(mHandler,UfhData.scanResult6c,1,"0001");
			}else{
				Toast.makeText(MaterialOutActivity.this,"请先结束当前任务!",Toast.LENGTH_SHORT).show();
			}
			break;
		case R.id.deleteMoPackage://删除
			deletePackage();
			break;
		case R.id.mo_refresh://刷新
			refreshList();
			break;
		case R.id.mo_checkIt_bt://确定
			if("-1".equals(lastBt)){
				if(materialAddData.size()>0||materialDelData.size()>0){
					lastBt="4";//确定
					CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
					Map m=new HashMap();
					m.put("extendingMaterialId", extendingMaterialId);
					m.put("delExtendingInfoId", StringUtils.join(materialDelData.toArray(), ","));
					m.put("operateType", operateType);
					m.put("user_id", UfhData.getEmployBean().getId());
					m.put("materialScanCode", StringUtils.join(materialAddData.toArray(), ","));
					checkDataFromServer.setData(m);
					checkDataFromServer.setOperType("7");//保存数据
					checkDataFromServer.setIp(UfhData.getIP());
					checkDataFromServer.setmHandler(mHandler);
					checkDataFromServer.setWhat(4);
					checkDataFromServer.checkData();
				}else{
					Toast.makeText(MaterialOutActivity.this,"未进行任何数据操作!",Toast.LENGTH_SHORT).show();
				}
			}else{
				Toast.makeText(MaterialOutActivity.this,"当前任务未结算，请稍等!",Toast.LENGTH_SHORT).show();
			}
			break;
		default:
			break;
		}
		return;
	}
	private boolean checkMaterial(Map<String,Integer> map1){
		boolean type=false;
		for(String key:map1.keySet()){  
			if(materiaCheckData.get(materialListData.get(key))!=null){
				if("未扫描".equals(materiaCheckData.get(materialListData.get(key)))){
					materiaCheckData.put(materialListData.get(key), "已扫描");
					materialAddData.add(key);
					type=true;
				}
			}
		}
		counts=0;
		for(Object key:materiaCheckData.keySet()){  
			if(!"未扫描".equals(materiaCheckData.get(key))){
				counts++;
			}
		}
		return type;
	}
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
	}

	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		return super.onKeyDown(keyCode, event);
	}
	@Override
	protected void showScanCode() {
		// TODO Auto-generated method stub
		super.showScanCode();
//		Toast.makeText(ProductOutActivity.this,getBarcodeStr(),Toast.LENGTH_SHORT).show();
		data.put(getBarcodeStr(), 1);
		if(!checkMaterial(data)){
			List checkList=checkMapData(oldData,data);
			if(checkList.size()>0){
				CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
				String s=StringUtils.join(checkList.toArray(), ",");
				Map m=new HashMap();
				m.put("scanCode", s);
				m.put("extendingMaterialId", extendingMaterialId);
				m.put("user_id", UfhData.getEmployBean().getId());
				checkDataFromServer.setOperType("15");
				checkDataFromServer.setData(m);
				checkDataFromServer.setIp(UfhData.getIP());
				checkDataFromServer.setmHandler(handler);
				checkDataFromServer.setWhat(1);
				oldData.putAll(data);
				if(!checkDataFromServer.checkData()){//此处有问题
					oldData.remove(data);
				}
			}
		}else{
			if(checkAdapter == null){
				checkAdapter = new CheckBoxAdapter(MaterialOutActivity.this, materiaCheckData);
				listView.setAdapter(checkAdapter);
			}else{
				checkAdapter.setData(materiaCheckData) ;
			}
			allCounts.setText(counts+"");
			checkAdapter.notifyDataSetChanged();
		}
	}
	private String checkResult(Map map,Map map1){
		List list=new ArrayList<String>();
		for(Object key:map1.keySet()){  
			if("已扫描".equals(map1.get(key))){
				list.add(getKey(map, key.toString()));
			}
		} 
		String s=StringUtils.join(list.toArray(), ",");
		return s;
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, MaterialOutListActivity.class);  
        startActivity(intent);
        finish();
	}
	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
        		try {
        			String s= (String) msg.obj;
                    switch (msg.what) {
                    case 0:
        				if(s==null||"-1".equals(s)){
    						Toast.makeText(MaterialOutActivity.this,"单据号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
        							Map<Object,Object> m=new HashMap<Object,Object>();
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                types=obj.getString("isChecked");
    				                if("0".equals(types)){
    				                	types="未扫描";
    				                }else if("1".equals(types)){
    				                	types="已扫描";
    				                	counts++;
    				                }
    				                try {
    				    				m.put("packageInventoryNum", obj.getString("packageInventoryNum"));//流水号
    				    				m.put("scanCode", obj.getString("scanCode"));//条码
    				    				m.put("isChecked", obj.getString("isChecked"));//原包装状态 
    				    				materialOriginData.put(obj.getString("packageInventoryNum"),m);
    				                	materialListData.put(obj.getString("scanCode"),obj.getString("packageInventoryNum"));
    				                	materiaCheckData.put(obj.getString("packageInventoryNum"), types);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    							oldData.putAll(data);
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(checkAdapter == null){
    							checkAdapter = new CheckBoxAdapter(MaterialOutActivity.this, materiaCheckData);
    							listView.setAdapter(checkAdapter);
    						}else{
    							checkAdapter.setData(materiaCheckData) ;
    						}
    						allCounts.setText(counts+"");
    						checkAdapter.notifyDataSetChanged();
    					}
        				break;
    				case 1:
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(MaterialOutActivity.this,"扫描有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							String result=ss.getString("result");
    							if("100".equals(result)){
    	    						Toast.makeText(MaterialOutActivity.this,"该包装需要称量!",Toast.LENGTH_SHORT).show();
    	            				break;
    							}else if("7".equals(result)){
    	    						Toast.makeText(MaterialOutActivity.this,"该物料不在推荐批次中!",Toast.LENGTH_SHORT).show();
    							}else if("5".equals(result)){
    	    						Toast.makeText(MaterialOutActivity.this,"该包装不存在!",Toast.LENGTH_SHORT).show();
    							}else if("Locked".equals(result)){
    	    						Toast.makeText(MaterialOutActivity.this,"该包装批次已被锁定!",Toast.LENGTH_SHORT).show();
    							}else if("XX".equals(result)){
    								if(!ss.isNull("warningMessage")){
        	    						Toast.makeText(MaterialOutActivity.this,ss.getString("warningMessage"),Toast.LENGTH_SHORT).show();
    								}else{
        	    						Toast.makeText(MaterialOutActivity.this,"包装占用！",Toast.LENGTH_SHORT).show();
    								}
    							}
    							for (int i = 0; i< objList.length(); i++) {
        							Map<Object,Object> m=new HashMap<Object,Object>();
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                types=obj.getString("isChecked");
    				                if("0".equals(types)){
    				                	types="未扫描";
    				                }else if("1".equals(types)){
    				                	types="已扫描";
    				                	counts++;
    				                }
    				                try {
    				    				m.put("packageInventoryNum", obj.getString("packageInventoryNum"));//流水号
    				    				m.put("scanCode", obj.getString("scanCode"));//条码
    				    				m.put("isChecked", "-1");//原包装状态 
    				    				materialAddData.add(obj.getString("scanCode"));
    				    				materialOriginData.put(obj.getString("packageInventoryNum"),m);
    				                	materialListData.put(obj.getString("scanCode"),obj.getString("packageInventoryNum"));
    				                	materiaCheckData.put(obj.getString("packageInventoryNum"), types);
    								} catch (Exception e) {
    									// TODO: handle exception
    								}
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    						if(checkAdapter == null){
    							checkAdapter = new CheckBoxAdapter(MaterialOutActivity.this, materiaCheckData);
    							listView.setAdapter(checkAdapter);
    						}else{
    							checkAdapter.setData(materiaCheckData) ;
    						}
    						allCounts.setText(counts+"");
    						checkAdapter.notifyDataSetChanged();
    					}
        				break;
        			default:
        				break;
        			}
				} catch (Exception e) {
					// TODO: handle exception
					Toast.makeText(MaterialOutActivity.this,"操作错误!",Toast.LENGTH_SHORT).show();
				}
            }

    };
}
