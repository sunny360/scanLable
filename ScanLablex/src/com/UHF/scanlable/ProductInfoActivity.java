package com.UHF.scanlable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ActivityGroup;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

import com.UHF.R;
import com.UHF.model.AcceptanceAdapter;
import com.UHF.model.CheckBoxAdapter;
import com.UHF.util.CheckDataFromServer;

public class ProductInfoActivity extends Activity implements OnClickListener{

	Button button_write;

	EditText et_packageInventoryNums0;
	EditText et_materialCodes0;
	EditText et_names0;
	EditText et_specifications0;
	EditText et_batchNums0;
	EditText et_counts0;
	EditText et_statuss0;
	EditText et_positions0;

	
	Button button_back;
	/**
	 * 流水号/状态
	 */
	private LinkedHashMap<Object,Object> listData=new LinkedHashMap<Object,Object>();
	private CheckBoxAdapter checkAdapter;
	private Intent intent;
	private String scanCode;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.product_info);
		scanCode= getIntent().getStringExtra("scanCode");
		intent = new Intent(this,ProductIncomingActivity.class);

		et_packageInventoryNums0=(EditText)findViewById(R.id.et_packageInventoryNums0);
		et_materialCodes0=(EditText)findViewById(R.id.et_materialCodes0);
		et_names0=(EditText)findViewById(R.id.et_names0);
		et_specifications0=(EditText)findViewById(R.id.et_specifications0);
		et_batchNums0=(EditText)findViewById(R.id.et_batchNums0);
		et_statuss0=(EditText)findViewById(R.id.et_statuss0);
		et_positions0=(EditText)findViewById(R.id.et_positions0);
		et_counts0=(EditText)findViewById(R.id.et_counts0);

		refreshList();
		
	}
	public boolean refreshList(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("scanCode", scanCode);
		checkDataFromServer.setOperType("38");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(0);
		checkDataFromServer.checkData();
		return true;
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==button_write.getId()){
			submitPackage();
		}else if(v.getId()==button_back.getId()){
			onBackPressed();
		}
	}
	public boolean submitPackage(){
		CheckDataFromServer checkDataFromServer =new CheckDataFromServer();
		String inventoryCount=et_counts0.getText().toString();
		Map m=new HashMap();
		m.put("user_id", UfhData.getEmployBean().getId());
		m.put("inventoryCount", inventoryCount);//实际量
		m.put("scanCode", scanCode);
		checkDataFromServer.setOperType("1");//成品入库待复核单据
		checkDataFromServer.setData(m);
		checkDataFromServer.setIp(UfhData.getIP());
		checkDataFromServer.setmHandler(handler);
		checkDataFromServer.setWhat(1);
		checkDataFromServer.checkData();
		return true;
	}
	
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
		Intent intent = new Intent(this, QueryActivity.class); 
        startActivity(intent);
        finish();
	}
	
	


	private Handler handler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
        		super.handleMessage(msg);
                switch (msg.what) {
    			case 0:
    				try {
    					String s= (String) msg.obj;
    					if(s==null||"-1".equals(s)){
    						Toast.makeText(ProductInfoActivity.this,"流水号有误!",Toast.LENGTH_SHORT).show();
    					}else{
    						JSONArray objList;
    						try {
    		    				JSONObject ss= new JSONObject(s);
    		    				String operType=ss.getString("operType");
    							objList = new JSONArray(ss.getString("list"));
    							String types="";
    							for (int i = 0; i< objList.length(); i++) {
    				                //循环遍历，依次取出JSONObject对象
    				                //用getInt和getString方法取出对应键值
    				                JSONObject obj = objList.getJSONObject(i);
    				                /*acceptanceData.put(checkHex(Long.toHexString(Long.parseLong(obj.get("epcId").toString())).toUpperCase(),"0001"),obj.get("packageInventoryNum"));//epc,流水号
    				                String backGroundColor=obj.get("backGroundColor").equals("0")?"已复核":"未复核";
    				                acceptanceCheckData.put(obj.get("packageInventoryNum").toString(),backGroundColor);//epc,流水号*/
    				                JSONObject productStorageBean= obj.getJSONObject("productStorage");
    				                if(!(obj.isNull("packageInventoryNum")||"".equals(obj.get("packageInventoryNum")))){
    				                	et_packageInventoryNums0.setText(obj.getString("packageInventoryNum"));
    				                }
    				                if(!(obj.isNull("goodsCode")||"".equals(obj.get("goodsCode")))){
    				                	et_materialCodes0.setText(obj.getString("goodsCode"));
    				                }
    				                if(!(obj.isNull("goodsName")||"".equals(obj.get("goodsName")))){
    				                	et_names0.setText(obj.getString("goodsName"));
    				                }
    				                if(!(productStorageBean.isNull("specification")||"".equals(productStorageBean.get("specification")))){
    				                	et_specifications0.setText(productStorageBean.getString("specification"));
    				                }
    				                if(!(obj.isNull("batchProduction")||"".equals(obj.get("batchProduction")))){
    				                	et_batchNums0.setText(obj.getString("batchProduction"));
    				                }
    				                if(!(obj.isNull("wareHousePosCode")||"".equals(obj.get("wareHousePosCode")))){
    				                	et_positions0.setText(obj.getString("wareHousePosCode"));
    				                }
    				                if(!(obj.isNull("weightCount")||"".equals(obj.get("weightCount")))){
    				                	et_counts0.setText(obj.getString("weightCount"));
    				                }
    				                if(!(obj.isNull("qualityState")||"".equals(obj.get("qualityState")))){
    				                	et_statuss0.setText(obj.getString("qualityState"));
    				                }
    				             }
    						} catch (JSONException e) {
    							// TODO Auto-generated catch block
    							e.printStackTrace();
    						}
    					}
					} catch (Exception e) {
						// TODO: handle exception
						Toast.makeText(ProductInfoActivity.this,"流水号有误!",Toast.LENGTH_SHORT).show();
    				}
    				break;
    			case 1:
    				break;
    			case 2:
    				break;
    			default:
    				break;
    			}
            }

    };
}
